<?php

/**
 * @see       https://github.com/laminas/laminas-mvc-skeleton for the canonical source repository
 * @copyright https://github.com/laminas/laminas-mvc-skeleton/blob/master/COPYRIGHT.md
 * @license   https://github.com/laminas/laminas-mvc-skeleton/blob/master/LICENSE.md New BSD License
 */

/**
 * List of enabled modules for this application.
 *
 * This should be an array of module namespaces used in the application.
 */
return [
    'Laminas\Mvc\Plugin\FlashMessenger',
    'Laminas\Mvc\Plugin\FilePrg',
    'Laminas\Session',
    'Laminas\I18n',
    'Laminas\Db',
    'Zend\Cache',
    'Zend\Form',
    'Laminas\InputFilter',
    'Laminas\Filter',
    'Laminas\Paginator',
    'Laminas\Hydrator',
    'Laminas\Mvc\Console',
    'Laminas\Router',
    'Laminas\Validator',
    'DoctrineModule',
    'DoctrineORMModule',
    'Phpro\DoctrineHydrationModule',
    'Laminas\ApiTools',
    'Laminas\ApiTools\ApiProblem',
    'Laminas\ApiTools\Hal',
    'Laminas\ApiTools\Versioning',
    'Laminas\ApiTools\ContentNegotiation',
    'Laminas\ApiTools\Rpc',
    'Laminas\ApiTools\MvcAuth',
    'Laminas\ApiTools\Rest',
    'Laminas\ApiTools\ContentValidation',
    'Laminas\ApiTools\Admin',
    'Laminas\ApiTools\Doctrine\Admin',
    'Laminas\ApiTools\Doctrine\Server',
    'Laminas\ApiTools\Doctrine\QueryBuilder',
    'Laminas\ApiTools\Configuration',
    'Application',
];
