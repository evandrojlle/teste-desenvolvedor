<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Zend\\Cache\\' => array($vendorDir . '/zendframework/zend-cache/src'),
    'Webimpress\\SafeWriter\\' => array($vendorDir . '/webimpress/safe-writer/src'),
    'Symfony\\Polyfill\\Php80\\' => array($vendorDir . '/symfony/polyfill-php80'),
    'Symfony\\Polyfill\\Php73\\' => array($vendorDir . '/symfony/polyfill-php73'),
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Polyfill\\Intl\\Normalizer\\' => array($vendorDir . '/symfony/polyfill-intl-normalizer'),
    'Symfony\\Polyfill\\Intl\\Grapheme\\' => array($vendorDir . '/symfony/polyfill-intl-grapheme'),
    'Symfony\\Polyfill\\Ctype\\' => array($vendorDir . '/symfony/polyfill-ctype'),
    'Symfony\\Contracts\\Service\\' => array($vendorDir . '/symfony/service-contracts'),
    'Symfony\\Component\\String\\' => array($vendorDir . '/symfony/string'),
    'Symfony\\Component\\Console\\' => array($vendorDir . '/symfony/console'),
    'Psr\\SimpleCache\\' => array($vendorDir . '/psr/simple-cache/src'),
    'Psr\\Link\\' => array($vendorDir . '/psr/link/src'),
    'Psr\\Container\\' => array($vendorDir . '/psr/container/src'),
    'Psr\\Cache\\' => array($vendorDir . '/psr/cache/src'),
    'Phpro\\DoctrineHydrationModule\\' => array($vendorDir . '/phpro/zf-doctrine-hydration-module/src'),
    'PhpParser\\' => array($vendorDir . '/nikic/php-parser/lib/PhpParser'),
    'PackageVersions\\' => array($vendorDir . '/composer/package-versions-deprecated/src/PackageVersions'),
    'Laminas\\ZendFrameworkBridge\\' => array($vendorDir . '/laminas/laminas-zendframework-bridge/src'),
    'Laminas\\View\\' => array($vendorDir . '/laminas/laminas-view/src'),
    'Laminas\\Validator\\' => array($vendorDir . '/laminas/laminas-validator/src'),
    'Laminas\\Uri\\' => array($vendorDir . '/laminas/laminas-uri/src'),
    'Laminas\\Text\\' => array($vendorDir . '/laminas/laminas-text/src'),
    'Laminas\\Stdlib\\' => array($vendorDir . '/laminas/laminas-stdlib/src'),
    'Laminas\\Session\\' => array($vendorDir . '/laminas/laminas-session/src'),
    'Laminas\\ServiceManager\\' => array($vendorDir . '/laminas/laminas-servicemanager/src'),
    'Laminas\\Router\\' => array($vendorDir . '/laminas/laminas-router/src'),
    'Laminas\\Permissions\\Rbac\\' => array($vendorDir . '/laminas/laminas-permissions-rbac/src'),
    'Laminas\\Permissions\\Acl\\' => array($vendorDir . '/laminas/laminas-permissions-acl/src'),
    'Laminas\\Paginator\\' => array($vendorDir . '/laminas/laminas-paginator/src'),
    'Laminas\\Mvc\\Plugin\\FlashMessenger\\' => array($vendorDir . '/laminas/laminas-mvc-plugin-flashmessenger/src'),
    'Laminas\\Mvc\\Plugin\\FilePrg\\' => array($vendorDir . '/laminas/laminas-mvc-plugin-fileprg/src'),
    'Laminas\\Mvc\\Console\\' => array($vendorDir . '/laminas/laminas-mvc-console/src'),
    'Laminas\\Mvc\\' => array($vendorDir . '/laminas/laminas-mvc/src'),
    'Laminas\\ModuleManager\\' => array($vendorDir . '/laminas/laminas-modulemanager/src'),
    'Laminas\\Math\\' => array($vendorDir . '/laminas/laminas-math/src'),
    'Laminas\\Loader\\' => array($vendorDir . '/laminas/laminas-loader/src'),
    'Laminas\\Json\\' => array($vendorDir . '/laminas/laminas-json/src'),
    'Laminas\\InputFilter\\' => array($vendorDir . '/laminas/laminas-inputfilter/src'),
    'Laminas\\I18n\\' => array($vendorDir . '/laminas/laminas-i18n/src'),
    'Laminas\\Hydrator\\' => array($vendorDir . '/laminas/laminas-hydrator/src'),
    'Laminas\\Http\\' => array($vendorDir . '/laminas/laminas-http/src'),
    'Laminas\\Form\\' => array($vendorDir . '/laminas/laminas-form/src'),
    'Laminas\\Filter\\' => array($vendorDir . '/laminas/laminas-filter/src'),
    'Laminas\\EventManager\\' => array($vendorDir . '/laminas/laminas-eventmanager/src'),
    'Laminas\\Escaper\\' => array($vendorDir . '/laminas/laminas-escaper/src'),
    'Laminas\\DevelopmentMode\\' => array($vendorDir . '/laminas/laminas-development-mode/src'),
    'Laminas\\Db\\' => array($vendorDir . '/laminas/laminas-db/src'),
    'Laminas\\Crypt\\' => array($vendorDir . '/laminas/laminas-crypt/src'),
    'Laminas\\Console\\' => array($vendorDir . '/laminas/laminas-console/src'),
    'Laminas\\Config\\' => array($vendorDir . '/laminas/laminas-config/src'),
    'Laminas\\ComponentInstaller\\' => array($vendorDir . '/laminas/laminas-component-installer/src'),
    'Laminas\\Code\\' => array($vendorDir . '/laminas/laminas-code/src'),
    'Laminas\\Authentication\\' => array($vendorDir . '/laminas/laminas-authentication/src'),
    'Laminas\\ApiTools\\Versioning\\' => array($vendorDir . '/laminas-api-tools/api-tools-versioning/src'),
    'Laminas\\ApiTools\\Rpc\\' => array($vendorDir . '/laminas-api-tools/api-tools-rpc/src'),
    'Laminas\\ApiTools\\Rest\\' => array($vendorDir . '/laminas-api-tools/api-tools-rest/src'),
    'Laminas\\ApiTools\\Provider\\' => array($vendorDir . '/laminas-api-tools/api-tools-provider/src'),
    'Laminas\\ApiTools\\OAuth2\\' => array($vendorDir . '/laminas-api-tools/api-tools-oauth2/src'),
    'Laminas\\ApiTools\\MvcAuth\\' => array($vendorDir . '/laminas-api-tools/api-tools-mvc-auth/src'),
    'Laminas\\ApiTools\\Hal\\' => array($vendorDir . '/laminas-api-tools/api-tools-hal/src'),
    'Laminas\\ApiTools\\Doctrine\\QueryBuilder\\' => array($vendorDir . '/laminas-api-tools/api-tools-doctrine-querybuilder/src'),
    'Laminas\\ApiTools\\Doctrine\\' => array($vendorDir . '/laminas-api-tools/api-tools-doctrine/src'),
    'Laminas\\ApiTools\\ContentValidation\\' => array($vendorDir . '/laminas-api-tools/api-tools-content-validation/src'),
    'Laminas\\ApiTools\\ContentNegotiation\\' => array($vendorDir . '/laminas-api-tools/api-tools-content-negotiation/src'),
    'Laminas\\ApiTools\\Configuration\\' => array($vendorDir . '/laminas-api-tools/api-tools-configuration/src'),
    'Laminas\\ApiTools\\ApiProblem\\' => array($vendorDir . '/laminas-api-tools/api-tools-api-problem/src'),
    'Laminas\\ApiTools\\Admin\\' => array($vendorDir . '/laminas-api-tools/api-tools-admin/src'),
    'Laminas\\ApiTools\\' => array($vendorDir . '/laminas-api-tools/api-tools/src'),
    'Interop\\Container\\' => array($vendorDir . '/container-interop/container-interop/src/Interop/Container'),
    'Doctrine\\Persistence\\' => array($vendorDir . '/doctrine/persistence/lib/Doctrine/Persistence'),
    'Doctrine\\ORM\\' => array($vendorDir . '/doctrine/orm/lib/Doctrine/ORM'),
    'Doctrine\\Instantiator\\' => array($vendorDir . '/doctrine/instantiator/src/Doctrine/Instantiator'),
    'Doctrine\\Inflector\\' => array($vendorDir . '/doctrine/inflector/lib/Doctrine/Inflector'),
    'Doctrine\\DBAL\\' => array($vendorDir . '/doctrine/dbal/lib/Doctrine/DBAL'),
    'Doctrine\\Common\\Lexer\\' => array($vendorDir . '/doctrine/lexer/lib/Doctrine/Common/Lexer'),
    'Doctrine\\Common\\Inflector\\' => array($vendorDir . '/doctrine/inflector/lib/Doctrine/Common/Inflector'),
    'Doctrine\\Common\\Collections\\' => array($vendorDir . '/doctrine/collections/lib/Doctrine/Common/Collections'),
    'Doctrine\\Common\\Cache\\' => array($vendorDir . '/doctrine/cache/lib/Doctrine/Common/Cache'),
    'Doctrine\\Common\\Annotations\\' => array($vendorDir . '/doctrine/annotations/lib/Doctrine/Common/Annotations'),
    'Doctrine\\Common\\' => array($vendorDir . '/doctrine/common/lib/Doctrine/Common', $vendorDir . '/doctrine/event-manager/lib/Doctrine/Common', $vendorDir . '/doctrine/persistence/lib/Doctrine/Common', $vendorDir . '/doctrine/reflection/lib/Doctrine/Common'),
    'Brick\\VarExporter\\' => array($vendorDir . '/brick/varexporter/src'),
    'Application\\' => array($baseDir . '/module/Application/src'),
    'ApplicationTest\\' => array($baseDir . '/module/Application/test'),
);
