<?php

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;

require_once "vendor/autoload.php";


// INFORMANDO QUE NAO ESTA NO MODO DEVELOPMENT.
$isDevMode                 = false;
$proxyDir                  = null;
$cache                     = null;
$useSimpleAnnotationReader = false;
$paths                     = [__DIR__ . '/module/Application/src/Model']; // DIRETORIO DAS MODELS.

$dbParams             = require 'config/autoload/doctrine_orm.local.php';
$ormDefault           = $dbParams['doctrine']['connection']['orm_default']['params'];
$ormDefault['driver'] = 'pdo_mysql';

$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode, $proxyDir, $cache, $useSimpleAnnotationReader);

// CRIA UM NOVO ENTITYMANAGER QUE OPERA NA CONEXAO DE BANCO DE DADOS ESPECIFICADA
// E USA AS IMPLEMENTACOES DE CONFIGURACAO E EVENTMANAGER ESPECIFICADAS.
$entityManager = EntityManager::create($ormDefault, $config);

// CRIA AS ENTIDADES NO BANCO DE DADOS.
return ConsoleRunner::createHelperSet($entityManager);
