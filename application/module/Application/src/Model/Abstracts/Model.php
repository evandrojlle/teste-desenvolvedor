<?php

namespace Application\Model\Abstracts;

use Laminas\InputFilter\InputFilter;
use Laminas\InputFilter\InputFilterAwareInterface;
use Laminas\InputFilter\InputFilterInterface;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
    
/**
 * @ORM\MappedSuperclass
 */
abstract class Model implements InputFilterAwareInterface
{
    protected $salt = 'CAS REST SALT';

    protected $inputFilter;

    protected $entity_manager;

    public function __construct(\Doctrine\ORM\EntityManager $entityManager = null)
    {
        $this->entity_manager = $entityManager;
    }

    /**
     * Método Mágico setter para salvar propriedades protegidas.
     *
     * @param string $pName
     * @param mixed $pValue
     */
    public function __set($pName, $pValue)
    {
        $this->$pName = $pValue;
    }

    /**
     * Método Mágico getter para expor propriedades protegidas.
     *
     * @param string $pName
     * @return mixed
     */
    public function __get($pName)
    {
        return $this->$pName;
    }

    /**
     * Método Mágico call disparado quando invocando métodos inacessíveis em um contexto de objeto..
     *
     * @param string $pMethod
     * @param array $pArguments
     * @return mixed
     */
    public function __call($pMethod, $pArguments)
    {
        // Selecionando os 3 primeiros caracteres do Método invocado.
        $prefix = substr($pMethod, 0, 3);

        // Selecionando o restante do Método invocado.
        $prop = substr($pMethod, 3);

        // Converte os caracteres maúsculos por minúsculos, concatenando com underscore.
        $prop = preg_replace_callback(
            '/[A-Z]/',
            function ($matches) { return "_" . strtolower($matches[0]);},
            $prop
        );

        // Remove o primeiro underscore.
        if ($prop) {
            $prop = substr($prop, 1);
        }

        if ($prefix == 'set') {
            $this->$prop = $pArguments[0];
        } elseif ($prefix == 'get') {
            return $this->$prop;
        } else {
            throw new \Exception('O método ' . $pMethod . ' não existe!');
        }
    }
    
    public function clear(array $pArray)
    {
        unset($pArray['salt']);
        unset($pArray['inputFilter']);
        unset($pArray['entity_manager']);
        unset($pArray['controller']);
        unset($pArray['version']);
        unset($pArray['action']);
        unset($pArray['class']);
        unset($pArray['db']);

        return $pArray;
    }

    /**
     * Converte o objeto para array.
     *
     * @return array
     */
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
            
    public function setDatabaseNameDefault($pDatabaseName)
    {
        $this->database_name_default = $pDatabaseName;
    }
    
    public function getDatabaseNameDefault()
    {
        return $this->database_name_default;
    }
    
    public function passwordHash($pPassword)
    {
        if ($pPassword) {
            return md5($pPassword);
        }
    
        return false;
    }
    
    public function passwordVerify($pPassword, $pHash)
    {
        if ($pPassword && $pHash) {
            if (md5($pPassword) === $pHash) {
                return true;
            }
        }
    
        return false;
    }
    
    public function setEmailCodVerify($pStr, $pSalt = null)
    {
        if ($pSalt) {
            $str = $pStr . $pSalt . date('Y-m-d');
        } else {
            $str = $pStr . date('Y-m-d');
        }
    
        return $this->passwordHash($str);
    }

    public function getFilterByValue(array $pArray, string $pIndex, string $pValue, string $pOperator)
    {
        $newarray = array();
        if(is_array($pArray) && count($pArray) > 0)
        {
            foreach($pArray as $key => $val)
            {
                if($pOperator == 'LIKE')
                {
                    $valueSlashed = str_replace('/', '\/', $pValue);
                    preg_match("/{$valueSlashed}/", $pArray[$key][$pIndex], $matches, PREG_OFFSET_CAPTURE);
                    if($matches)
                        $newarray[$key] = $pArray[$key];
                }
                else
                {
                    if($pArray[$key][$pIndex] == $pValue)
                        $newarray[$key] = $pArray[$key];
                }
            }
        }

        return $newarray;
    }
    
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Não Implementado");
    }

    public function getInputFilter()
    {
    }

    public function iterateVisible()
    {
        $arrProperties = [];
        foreach ($this as $key => $value) {
            if ($key !== 'salt' &&
                $key !== 'inputFilter' &&
                $key !== 'entity_manager' &&
                $key !== 'class'
            ) {
                $arrProperties[] = $key;
            }
        }

        return $arrProperties;
    }

    protected function getArrayExpressions()
    {
        return [
            'eq',
            'neq',
            'lt',
            'lte',
            'gt',
            'gte',
            'isNull',
            'isNotNull',
            'prod',
            'diff',
            'sum',
            'quot',
            'exists',
            'all',
            'some',
            'any',
            'not',
            'in',
            'notIn',
            'like',
            'notLike',
            'between',
            'trim',
            'concat',
            'substring',
            'lower',
            'upper',
            'length',
            'avg',
            'max',
            'min',
            'abs',
            'sqrt',
            'count',
            'countDistinct',
            'and',
            'andx',
            'or',
            'orx',
        ];
    }
}
