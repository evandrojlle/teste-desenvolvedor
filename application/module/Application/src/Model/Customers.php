<?php 

namespace Application\Model;
        
use Application\Model\Abstracts\Model;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\EntityManager;
use DoctrineORMModuleTest\Assets\Entity\Date;
use Doctrine\ORM\Tools\Pagination\Paginator as DoctrinePaginator;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as PaginatorAdapter; 

use Laminas\Paginator\Paginator;
/**
 * @ORM\Entity
 * @ORM\Table(name="customers")
 * @property int $id
 * @property string $firstName
 * @property string $lastName
 * @property string $email
 * @property string $gender
 * @property string $ipAddress
 * @property string $company
 * @property string $city
 * @property string $title
 * @property string $website
 */
class Customers extends Model
{
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 */
	protected $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="first_name", type="string", length=33, nullable=false)
	 */
	protected $firstName;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="last_name", type="string", length=55, nullable=true)
	 */
	protected $lastName;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="email", type="string", length=75, nullable=false)
	 */
	protected $email;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="gender", type="string", length=6, nullable=true)
	 */
	protected $gender;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="ip_address", type="string", length=15, nullable=true)
	 */
    protected $ipAddress;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="company", type="string", length=255, nullable=false)
	 */
    protected $company;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="city", type="string", length=100, nullable=false)
	 */
    protected $city;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="title", type="string", length=55, nullable=false)
	 */
	protected $title;

	/**
	 * @var string
	 * 
	 * @ORM\Column(name="website", type="string", nullable=true)
	 */
	protected $website;

	/**
	 * @var string
	 *
	 * Class name
	 */
	protected $class;

	/**
	 * Construct class
	 */
	public function __construct(\Doctrine\ORM\EntityManager $entityManager)
	{
		parent::__construct($entityManager);

		$this->class = __CLASS__;
	}

	/**
	 * Get the value of id
	 *
	 * @return  int
	 */ 
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * Set the value of id
	 *
	 * @param  int  $id
	 *
	 * @return  self
	 */ 
	public function setId(int $id): self
	{
		$this->id = $id;

		return $this;
	}

	/**
	 * Get the value of firstName
	 *
	 * @return  string
	 */ 
	public function getFirstName(): string
	{
		return $this->firstName;
	}

	/**
	 * Set the value of firstName
	 *
	 * @param  string  $firstName
	 *
	 * @return  self
	 */ 
	public function setFirstName(string $firstName): self
	{
		$this->firstName = $firstName;

		return $this;
	}

	/**
	 * Get the value of lastName
	 *
	 * @return  string
	 */ 
	public function getLastName(): string
	{
		return $this->lastName;
	}

	/**
	 * Set the value of lastName
	 *
	 * @param  string  $lastName
	 *
	 * @return  self
	 */ 
	public function setLastName(string $lastName): self
	{
		$this->lastName = $lastName;

		return $this;
	}

	/**
	 * Get the value of email
	 *
	 * @return  string
	 */ 
	public function getEmail(): string
	{
		return $this->email;
	}

	/**
	 * Set the value of email
	 *
	 * @param  string  $email
	 *
	 * @return  self
	 */ 
	public function setEmail(string $email): self
	{
		$this->email = $email;

		return $this;
	}

	/**
	 * Get the value of gender
	 *
	 * @return  string
	 */ 
	public function getGender(): string
	{
		return $this->gender;
	}

	/**
	 * Set the value of gender
	 *
	 * @param  string  $gender
	 *
	 * @return  self
	 */ 
	public function setGender(string $gender): self
	{
		$this->gender = $gender;

		return $this;
	}

    /**
     * Get the value of ip_address
     *
     * @return  string
     */ 
    public function getIp_address(): string
    {
        return $this->ip_address;
    }

    /**
     * Set the value of ip_address
     *
     * @param  string  $ip_address
     *
     * @return  self
     */ 
    public function setIp_address(string $ip_address): self
    {
        $this->ip_address = $ip_address;

        return $this;
    }

    /**
     * Get the value of company
     *
     * @return  string
     */ 
    public function getCompany(): string
    {
        return $this->company;
    }

    /**
     * Set the value of company
     *
     * @param  string  $company
     *
     * @return  self
     */ 
    public function setCompany(string $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get the value of city
     *
     * @return  string
     */ 
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * Set the value of city
     *
     * @param  string  $city
     *
     * @return  self
     */ 
    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

	/**
	 * Get the value of title
	 *
	 * @return  string
	 */ 
	public function getTitle(): string
	{
		return $this->title;
	}

	/**
	 * Set the value of title
	 *
	 * @param  string  $title
	 *
	 * @return  self
	 */ 
	public function setTitle(string $title): self
	{
		$this->title = $title;

		return $this;
	}

	/**
	 * Get the value of website
	 *
	 * @return  string
	 */ 
	public function getWebsite(): string
	{
		return $this->website;
	}

	/**
	 * Set the value of website
	 *
	 * @param  string  $website
	 *
	 * @return  self
	 */ 
	public function setWebsite(string $website): self
	{
		$this->website = $website;

		return $this;
	}

	/**
	 * Get the value of class
	 *
	 * @return  string
	 */ 
	public function getClass(): string
	{
		return $this->class;
	}

	/**
	 * Set the value of class
	 *
	 * @param  string  $class
	 *
	 * @return  self
	 */ 
	public function setClass(string $class): self
	{
		$this->class = $class;

		return $this;
	}

	/**
	 * Returns a line according to parameters entered 
	 *
	 * @param array $pParams
	 *
	 * @return array;
	 */
	public function fetchRowBy(array $pParams): array
    {
    	$fetchRow = $this->entity_manager->getRepository($this->class)->findOneBy($pParams);
    	if (! $fetchRow) {
            return [];
        }

        return $this->clear($fetchRow->getArrayCopy());
    }

	/**
	 * Returns a line according to parameters entered 
	 *
	 * @param array $pParams
	 *
	 * @return array;
	 */
    public function fetchRowsBy(array $pParams): array
    {
    	$fetchRows = $this->entity_manager->getRepository($this->class)->findBy($pParams);
    	if (! $fetchRows) {
    		return [];
    	}

    	foreach ($fetchRows as &$fetchRow) {
            $fetchRow = $this->clear($fetchRow->getArrayCopy());
        }

        $rows['count'] = count($fetchRows);
        $rows['items'] = $fetchRow;

    	return $rows;
    }

    /**
     * Returns all non-deleted records with pagination
     *
     * @return \Laminas\Paginator\Paginator
     */
    public function paginator()
    {
        $dql = $this->getEntityManager()
        ->createQueryBuilder()
        ->select('c')
        ->from('Application\Model\Customers', 'c')
        ->orderBy('c.id', 'ASC');
        
        $doctrinePaginator = new DoctrinePaginator($dql);
        $paginatorAdapter = new PaginatorAdapter($doctrinePaginator);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->getRoute());
        $paginator->setItemCountPerPage(10);
        $paginator->setPageRange(5);

        return $paginator;
    }

    /**
     * Return all records with or without lastname
     *
     * @param boolean $pIsEmty
     * @return array
     */
    public function toLastName(bool $pIsEmty = false) : array
    {
        $compare = $pIsEmty ? '!=' : '=';
        $dql = $this->entity_manager->createQueryBuilder()
        ->select('c.id')
        ->from($this->class, 'c')
        ->where("c.lastName {$compare} ''");
		
        $query = $dql->getQuery();
        $fetchRows = $query->getResult();
				
        return $fetchRows;
    }

    /**
     * Return all records with valid or invalid email
     *
     * @param boolean $pIsValid
     * @return array
     */
    public function toEmail(bool $pIsValid = false) : array
    {
        $compare = $pIsValid ? 'REGEXP' : 'NOT REGEXP';
        $sql = "SELECT id
        FROM customers
        WHERE email {$compare} ?";
        $query = $this->entity_manager->getConnection()->prepare($sql);
        $query->execute(['^([0-9a-zA-Z]+([_.-]?[0-9a-zA-Z]+)*@[0-9a-zA-Z]+[0-9,a-z,A-Z,.,-]*(.){1}[a-zA-Z]{2,4})+$']);
        $fetchRows = $query->fetchAllAssociative();
				
        return $fetchRows;
    }

    /**
     * Return all records with or without gender
     *
     * @param boolean $pIsEmty
     * @return array
     */
    public function toGender(bool $pIsEmty = false) : array
    {
        $compare = $pIsEmty ? '!=' : '=';
        $dql = $this->entity_manager->createQueryBuilder()
        ->select('c.id')
        ->from($this->class, 'c')
        ->where("c.gender {$compare} ''");
		
        $query = $dql->getQuery();
        $fetchRows = $query->getResult();
				
        return $fetchRows;
    }

	/**
	 * Returns all records
	 * 
	 * @return array
	 */
    public function fetchAll(): array
    {
		$dql = "SELECT i FROM {$this->class} i";
		$query = $this->entity_manager->createQuery($dql);
        $fetchRows = $query->getArrayResult();
        if (! $fetchRows) {
            return [];
        }

        return [
            'count' => count($fetchRows),
            'items' => $fetchRows,
        ];

    }

	/**
	 * Returns all records in array ids
	 *
	 * @param array $pArrIds
	 *
	 * @return array
	 */
	public function fetchRowByInId(array $pArrIds): array
	{
		$strIds = implode(',', $pArrIds);
		$dql = "SELECT i FROM $this->class i WHERE i.id IN ($strIds)";
		$query = $this->entity_manager->createQuery($dql);
		$fetchRows = $query->getArrayResult();
		if (! $fetchRows) {
			return [];
        }

        return [
            'count' => count($fetchRows),
            'items' => $fetchRows,
        ];
	}

    /**
     * Load data from csv file
     *
     * @param string $pFile
     * @return boolean
     */
    public function saveLoadDataInFile(string $pFile): bool
    {
        $sql = "LOAD DATA INFILE '{$pFile}' INTO TABLE customers
        FIELDS TERMINATED BY ',' 
        ENCLOSED BY '\"' 
        LINES TERMINATED BY '\r\n'
        IGNORE 1 LINES
        (id, first_name, last_name, email, gender, ip_address, company, city, title, website);";

        $query = $this->entity_manager->getConnection()->prepare($sql);
        return $query->execute();
    }
}
