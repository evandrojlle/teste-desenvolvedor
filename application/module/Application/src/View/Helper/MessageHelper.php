<?php
namespace Application\View\Helper;
      
use Zend\View\Helper\AbstractHelper;  

/**
 * Helper para exibição de mensagens.
 */
class MessageHelper extends AbstractHelper{
    public function __invoke($messages){
        $viewHelper = "";
        if($messages){
            foreach($messages as $message){
                switch($message['code']){
                    case '1':
                        $alert = 'success';
                        $prefix = 'SUCESSO';
                    break;

                    case '2':
                        $alert = 'info';
                        $prefix = 'INFO';
                    break;

                    case '3':
                        $alert = 'warning';
                        $prefix = 'ATENÇÃO';
                    break;

                    case '4':
                        $alert = 'danger';
                        $prefix = 'ERRO';
                    break;

                    default:
                        $alert = 'success';
                        $prefix = 'SUCESSO';
                    break;
                }

                $viewHelper .= "
                    <div class='alert alert-{$alert} alert-dismissible fade show' role='alert'>
                        <strong>{$prefix}!</strong> {$message['message']}
                        <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                        <span aria-hidden='true'>&times;</span>
                        </button>
                    </div>
                ";
            }
        }

        return $viewHelper;
    }
} 
