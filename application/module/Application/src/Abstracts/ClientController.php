<?php

namespace Application\Abstracts;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Session\Config\StandardConfig;
use Laminas\Session\SessionManager;

class ClientController extends AbstractActionController
{
  /**
   * RETORNA OS SERVICOS DO LAMINAS.
   *
   * @return object
   */
  public function getServiceLocator()
  {
    return $this->getEvent()->getApplication()->getServiceManager();// SERVICOS DO LAMINAS
  }

  /**
   * RETORNA AS CONFIGURACOES DO APP
   *
   * @return array
   */
  public function getConfig()
  {
    // CONFIGURACOES CRIADAS NA module.config.
    return $this->getServiceLocator()->get('Config');
  }

  /**
   * CRIA A ENTIDADE DE CONEXAO COM SQLSERVER, USANDO DOCTRINE
   *
   * @return \Doctrine\ORM\EntityManager
   */
  public function getEntityManager()
  {
    // DIRETORIO DAS MODELS.
    $paths = [__DIR__ . '../../Model'];

    // CARREGANDO AS CONFIGURACOES DE BANCO DE DADOS NA config/autoload/doctrine_orm.
    $entitymanager = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');

    // INFORMANDO QUE NAO ESTA NO MODO DEVELOPMENT.
    $isDevMode = false;

    // ADICIONANDO A CLASSE DRIVER AOS PARAMETROS.
    $dbParams = $entitymanager->getConnection()->getParams();

    // CRIANDO AS CONFIGURACOES DO DOCTRINE PARA TER COMPATIBILIDADE COM DIVERSAS INSTALACOES.
    $setup = \Doctrine\ORM\Tools\Setup::createConfiguration($isDevMode);

    // CONTEINER DE CONFIGURACAO PARA TODAS AS OPCOES DE CONFIGURACAO DO DOCTRINE.
    // ELE COMBINA TODAS AS OPCOES DE CONFIGURACAO DO DBAL & ORM.
    // NOTA INTERNA: AO ADICIONAR UMA NOVA OPCAO DE CONFIGURACAO, BASTA ESCREVER UM PAR DE GETTER/SETTER.
    $configuration = $this->getServiceLocator()->get('doctrine.configuration.orm_default');

    // OBTEM A ESTRATEGIA PARA GERAR AUTOMATICAMENTE AS CLASSES DE PROXY.
    $autoGenerateProxy = $configuration->getAutoGenerateProxyClasses();

    // INSERINDO A ESTRATÉGIA PARA GERACAO AUTOMATICA DAS CLASSES DE PROXY NO SETUP TOOLS.
    $setup->setAutoGenerateProxyClasses($autoGenerateProxy);

    // DEFININDO O DIRETORIO PARA GERACAO AUTOMATICA DAS CLASSES DE PROXY.
    $setup->setProxyDir($configuration->getProxyDir());

    // DEFININDO O NAMESPACE DAS CLASSES DE PROXY.
    $setup->setProxyNamespace($configuration->getProxyNamespace());

    // INSTANCIANDO A CLASSE AnnotationReader, QUE REALIZA A LEITURa DAS ANOTAÇÕES DO DOCBLOCK.
    $annotationReader = new \Doctrine\Common\Annotations\AnnotationReader();

    // INSTANCIANDO A CLASSE AnnotationDriver.
    // REALIZA A LEITURA DOS METADADOS DE MAPEAMENTO DAS ANOTAÇÕES DO DOCBLOCK.
    $driver = new \Doctrine\ORM\Mapping\Driver\AnnotationDriver($annotationReader, $paths);

    // REGISTRANDO O CARREGAMENTO AUTOMATICO DAS ANOTACOES. MUITO PARECIDO COM spl_autoload_register().
    \Doctrine\Common\Annotations\AnnotationRegistry::registerLoader('class_exists');

    // DEFINE A IMPLEMENTACAO DO DRIVER DE CACHE USADA PARA O ARMAZENAMENTO EM CACHE DE METADADOS.
    $setup->setMetadataDriverImpl($driver);

    // CRIA UM NOVO ENTITYMANAGER QUE OPERA NA CONEXAO DE BANCO DE DADOS ESPECIFICADA
    // E USA AS IMPLEMENTACOES DE CONFIGURACAO E EVENTMANAGER ESPECIFICADAS.
    $entityManager = \Doctrine\ORM\EntityManager::create($dbParams, $setup);

    return $entityManager;
  }
}
