<?php
namespace Application\Forms;

use Laminas\Form\Element;
use Laminas\Form\Form;
use Laminas\Filter;
use Laminas\InputFilter;
use Laminas\Validator;


class UploadForm extends Form
{
    public function __construct($name = null, $options = [])
    {
        parent::__construct($name, $options);
        $this->addElements();
        $this->addInputFilter();
    }

    public function addElements()
    {
        $inputFile = new Element\File('csv_load_data');
        $inputFile->setLabel('Selecione o arquivo')->setAttribute('id', 'csv_load_data');
        $inputFile->setAttribute('class', 'form-control-file');
        $inputFile->setAttribute('accept', '.csv');
        $this->add($inputFile);
    }

    public function addInputFilter()
    {
        $inputFilter = new InputFilter\InputFilter();
        $inputFile = new InputFilter\FileInput('csv_load_data');
        $inputFile->setRequired(true);
        $inputFile->getValidatorChain()
        ->attach(new Validator\File\Extension(['extension' => ['csv'], 'case' => false, 'allowNonExistentFile' => true]));

        $inputFile->getFilterChain()
        ->attach(new Filter\File\RenameUpload([
            'target'    => '../data/tmp/customers.csv',
            'randomize' => true,
        ]));

        $inputFilter->add($inputFile);

        $this->setInputFilter($inputFilter);
    }
}
