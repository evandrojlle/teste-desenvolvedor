<?php
namespace Application\Controller;

class IndexControllerFactory
{
    public function __invoke($services)
    {
        return new IndexController($services);
    }
}
