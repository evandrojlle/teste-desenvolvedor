<?php
declare(strict_types=1);

namespace Application\Controller;

use Application\Abstracts\ClientController;
use Application\Forms\UploadForm;
use Application\Model\Customers;

use Doctrine\ORM\Query;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\EntityManager;
use DoctrineORMModuleTest\Assets\Entity\Date;
use Doctrine\ORM\Tools\Pagination\Paginator as DoctrinePaginator;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as PaginatorAdapter; 

use Laminas\View\Model\ViewModel;
use Laminas\Paginator\Paginator;

class IndexController extends ClientController
{
  public function indexAction()
  {
    $model = new Customers($this->getEntityManager());
    $model->setRoute($this->params()->fromRoute('page'));
    $paginator = $model->paginator(); // Todos os registros de forma paginada.

    $noLastName   = count($model->toLastName());
    $withLastName = count($model->toLastName(true));
    $validEmail   = count($model->toEmail(true));
    $invalidEmail = count($model->toEmail());
    $noGender     = count($model->toGender());
    $withGender   = count($model->toGender(true));
    $arrData      = [
      'paginator'     => $paginator,
      'count'         => $paginator->getTotalItemCount(),
      'currentItems'  => $paginator->getItemCountPerPage(),
      'currentPage'   => $paginator->getCurrentPageNumber(),
      'firstItem'     => ($paginator->getCurrentPageNumber() * $paginator->getItemCountPerPage()) - $paginator->getItemCountPerPage() + 1,
      'noLastName'    => $noLastName,
      'withLastName'  => $withLastName,
      'validEmail'    => $validEmail,
      'invalidEmail'  => $invalidEmail,
      'noGender'      => $noGender,
      'withGender'    => $withGender
    ];

    return new ViewModel($arrData);
  }

  public function loadDataAction()
  {
    $messages = [];
    $form     = new UploadForm('upload-form');
    $request  = $this->getRequest();
    if ($request->isPost()) {
      $post = array_merge_recursive(
          $request->getPost()->toArray(),
          $request->getFiles()->toArray()
      );

      $form->setData($post);
      if ($form->isValid()) {
        $inputData = $form->getData();
        $data      = $inputData['csv_load_data'];
        $tmpName   = substr($data['tmp_name'], 3);
        $path      = substr(__DIR__, 0, strpos(__DIR__, 'application'));
        $file      = str_replace('\\', '/', $path . $tmpName);
        try {
          $model = new Customers($this->getEntityManager());
          if ($model->saveLoadDataInFile($file)) {
            $messages = [
              'messages'  => [
                'success' => true,
                'message' => 'Carga efetuada com sucesso!',
                'code'    => '1'
              ]
            ];

          }
        } catch (\Exception $e) {
          $messages = [
            'messages'  => [
              'success' => false,
              'message' => $e->getMessage(),
              'code'    => '4'
            ]
          ];
        }

        unlink($file);
      } else {
          $formGet = $form->get('csv_load_data');
          $formGet->setAttribute('style', 'border:#FF0000 solid 1px');
          $formGet->setMessages(['fileUploadFileErrorNoFile' => 'Envie um arquivo .csv para carga de dados!']);
      }
    }

    return ['form' => $form, 'messages'	=> $messages,];
  }
}
