# Projeto de teste para vaga de desenvolvedor

## Introduction

Este � apenas um projeto para o teste de desenvolvedor aplicado e solicitado pela Giver.

## Installa��o

Neste projeto utilizei o framework Laminas. Laminas � o novo 'Dono' do Zendframework, ZendExpressive e Apigility. Utilizei tamb�m o Doctrine como mapeamento de objeto relacional para conex�o e acesso ao banco de dados. Acredito no Doctrine por v�rios fatores, mas principalmente pela integrabilidade e facilidade de conex�o com v�rios bancos de dados. J� o utilizei com mySql, PostgreSql e atualmente com SqlServer, executando perfeitamente suas tarefas.

Ap�s este projeto ser baixado do git, para seu funcionamento ser� necess�rio realizar as configura��es no servidor local.

## PHP
Para o correto funcionamento e desenvolvimento do projeto, a vers�o m�nima do PHP dever ser a vers�o 7.3.x. Vers�es inferiores causar�o erros no projeto em ambiente **local** e erro de desenvolvimento em **development** e **production**. 

##### Exemplo #1 Vers�o do PHP
```
PHP 7.4.11 (cli) (built: Sep 29 2020 13:18:06) ( ZTS Visual C++ 2017 x64 )
Copyright (c) The PHP Group
Zend Engine v3.4.0, Copyright (c) Zend Technologies
```
#### Extens�es ativas do PHP

###### PHP Modules
1. bcmath
2. bz2
3. calendar
4. Core
5. ctype
6. curl
7. date
8. dom
9. exif
10. fileinfo
11. filter
12. ftp
13. gd
14. gettext
15. hash
16. iconv
17. intl
18. json
19. libxml
20. mbstring
21. mysqli
22. mysqlnd
23. openssl
24. pcre
25. PDO
26. pdo_mysql
27. Phar
28. readline
29. Reflection
30. session
31. SimpleXML
32. soap
33. SPL
34. standard
35. tokenizer
36. xml
37. xmlreader
38. xmlwriter
39. zip
40. zlib

## Mysql/MariaDB

Abaixo est�o especificados as configura��es do Mysql/MariaDB.

1. Servidor: 127.0.0.1 via TCP/IP
2. Tipo de servidor: MariaDB
3. Conex�o com o servidor: SSL n�o est� sendo usado Documenta��o
4. Vers�o do servidor: 10.4.14-MariaDB - mariadb.org binary distribution
5. Vers�o do protocolo: 10
6. Utilizador: root@localhost
8. Conjunto de caracteres do servidor: cp1252 West European (latin1)

## Apache
J� existe um arquivo com o vhost configurado no arquivo httpd.conf existente na pasta vhost.

##### Exemplo #2 Configura��o vhost
```bash
NameVirtualHost 127.0.0.1:80
<VirtualHost 127.0.0.1:80>
	ServerAdmin admin@localhost
	ServerName giver.dev.br
	DocumentRoot "/evandro/projetos/giver/teste-desenvolvedor/application/public"
	<Directory "/evandro/projetos/giver/teste-desenvolvedor/application/public">
		DirectoryIndex index.php
		AllowOverride All
		Order allow,deny
		Allow from all
	</Directory>
</VirtualHost>
```
Modifique o endere�o em ServerName e o caminho do projeto existente em DocumentRoot e Directory.
Ap�s essa modifica��o, inclua-o em seu arquivo vhosts do Apache, que geralmente fica em apache/conf/extra/httpd-vhosts.conf

##### Exemplo #3 Inclus�o no vhosts do Apache
```bash
Include "/evandro/projetos/giver/teste-desenvolvedor/vhost/httpd.conf"
```
Passando o caminho do seu arquivo httpd.conf no seu servidor local.

Configure o arquivo hosts para apontar para o endere�o especificado em ServerName do seu vhost.

##### Exemplo #4 Inclus�o no host do servidor
```bash
127.0.0.1 	giver.dev.br
```

### Servidor Embutido do PHP.

O PHP, a partir da vers�o 5.3 possui um servidor embutido, sendo uma alternativa ao Apache. O servidor embutido foir criado para ser utilizado somente em ambientes de desenvolvimento, n�o sendo recomendados para ambientes de produ��o.

##### Exemplo #5 Acesso atrav�s do servidor embutido do PHP
```bash
$ cd /evandro/projetos/giver/teste-desenvolvedor/application
$ php -S 0.0.0.0:8080 -t public

# Ou use o composer alias:
$ composer run --timeout 0 serve
```

Isso ir� iniciar o cli-server na porta 8080 e deix�-lo dispon�vel para utiliza��o. Voc� pode ent�o visitar o site em http://localhost:8080/ - que abrir� a p�gina home do projeto.

##### Exemplo #6 Executando o servidor embutido atrav�s do composer.

O Laminas, por padr�o, possui uma configura��o ciada no arquivo *composer.json*, que facilita a utiliza��o deste comando. O comando *composer serve*, � um apelido (alias) que executar�, por baixo dos panos, o comando *php -S...*

##### Exemplo #7 Executando o servidor embutido atrav�s do composer.
```
composer serve
```

**Note:** O servidor embutido do PHP *deve ser usado somente em desenvolvimento*.

### Modo de desenvolvimento

Por padr�o o Laminas MVC � iniciado com a configura��o [laminas-development-mode] (https://github.com/laminas/laminas-development-mode) ativa, e fornece tr�s aliases para consumir o script que acompanha:

```bash
$ composer development-enable  # Ativa o modo de desenvolvimento
$ composer development-disable # Desativa o modo de desenvolvimento
$ composer development-status  # Verifica se o modo de desenvolvimento est� ativo
```

## Iniciando os trabalhos.

Ap�s toda a configura��o do servidor, PHP e carregando o projeto no servidor, � necess�rio configurar o banco de dados.

### Criando o banco de dados.
Crie no Mysql/MariaDB o banco de dados para a carga do projeto.

##### Exemplo #8 Cria��o do banco de dados.
```bash
$ mysql -u root # conectando ao Mysql/MariaDB

# Mensagem de boas vindas
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 639
Server version: 10.4.14-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

# Prompt dispon�vel para comandos
$ MariaDB [(none)]> Create database giver; # Comando para criar o banco de dados

# Mensagem de sucesso.
Query OK, 1 row affected (0.005 sec)

# Prompt dispon�vel para novos comandos
MariaDB [(none)]>
```

### Configurando o banco no projeto.

Neste projeto, para administra��o e conex�o como o banco de dados utilizei o Doctrine ORM que � um dos projetos mais importantes do conjunto de projetos Doctrine. Ele � um ORM implementado usando o padr�o Data Mapper e pode ser usado desde pequenos projetos at� grandes aplica��es. Pode ser usado em conjunto com grandes frameworks PHP como Symfony, Laravel, Laminas (Zend), Slim e outros.

Para configurar o banco de dados, v� at� o diret�rio application/config/autoload e edite o arquivo doctrine_orm.local.php. Este arquivo retorna apenas um array com as configura��es do banco de dados.

altere o host (linha 8), dbname (linha 10), user (linha 11) e password (linha 12) conforme seu ambiente local ou servidor.

##### Exemplo #9 Configura��o do banco de dados.
```bash
<?php
return [
	'doctrine' => [
		'connection' => [
			'orm_default' => [
				'driverClass' => 'Doctrine\DBAL\Driver\PDOMySql\Driver',
				'params' => [
					'host' => 'localhost',
					'port' => '3306',
					'dbname' => 'giver',
					'user' => 'evandrojlle',
					'password' => '123456',
				],
			],
		],
		'configuration' => [
			'orm_default' => [
				'generate_proxies' => false,
				'proxy_dir'		   => './data/doctrine/proxies',
				'proxy_namespace'  => 'Proxies',
			],
		],
	],
];

```

### Criando a tabela Customer

O Doctrine disponibiliza um comando para que seja poss�vel a cria��o da(s) tabela(s) atrav�s do pr�prio Doctrie.

Ap�s a configura��o da conex�o com Mysql/MariaDB e a cria��o da model, em application/module/Appication/src/Model/Customer.php, conforme os padr�es estabelecidos pelo Doctrine � poss�vel gerar as tabelas de forma autom�tica executando o comando:

```bash
$ vendor/bin/doctrine orm:schema-tool:create
```

**Note:** O usu�rio que est� conectado no projeto deve ter privil�gios para executar essa a��o.

Se preferir, a tabela **customers** pode ser criada manualmente.

```bash
CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(33) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_address` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(55) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1001 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci
```

A partir de agora tudo deve estar funcionando e atraves da url criada [giver.dev.br] (http://giver.dev.br) deve ser poss�vel o acesso ao projeto.

### A lista de Clientes

A lista de clientes � apresentada na p�gina home do projeto que mostrar� a lista paginada de clientes e as quantidades de clientes com e sem sobrenome, com e-mail v�lidos e inv�lidos e com e sem g�nero.

## A carga de dados.
A carga de dados � feita atrav�s do menu 'Import Data' que ser� respons�vel por inserir as informa��oes na tabela customer no banco de dados. A carga dos dados foi feita atrav�s de upload do arquivo existente em data/customers.csv e execu��o do comando LOAD DATA INFILE.

##### Obrigado!
##### Qualquer D�vida estou a Disposi��o.
